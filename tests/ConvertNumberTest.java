import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 */

/**
 * @author elstamey
 *
 */
public class ConvertNumberTest {

	@Test
	public void testMainMethodFailsWithNoInput() {
		String expected = "Please provide a number";
		String failMsg = "Unexpected response when no arguments";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod((String[])null));
	}

	@Test
	public void testMainMethodFailsWithLetters() {
		String expected = "Please provide an integer between -999,999 and 999,999";
		String failMsg = "Unexpected response when no arguments";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"1a0000"}));
	}
	
	@Test
	public void testMainMethodFailsWithNumberOneMillionOrMore() {
		String expected = "Please provide an integer between -999,999 and 999,999";
		String failMsg = "Unexpected response for 1000000";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"1000000"}));
	}

	@Test
	public void testMainMethodFailsWithNumberNegativeOneMillionOrLess() {
		String expected = "Please provide an integer between -999,999 and 999,999";
		String failMsg = "Unexpected response for -1000000";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"-1000000"}));
	}
	
	@Test
	public void testMainMethodPassesWithNumberNineHundredNinetyNineThousandTwoHundredTwentyOne() {
		String expected = "nine hundred ninety nine thousand two hundred twenty one";
		String failMsg = "Unexpected response for 999221";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"999221"}));
	}	
	
	@Test
	public void testMainMethodPassesWithNumberNineHundredNinetyNineThousandNineHundredAndNinetyNine() {
		String expected = "nine hundred ninety nine thousand nine hundred ninety nine";
		String failMsg = "Unexpected response for 999999";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"999999"}));
	}

	@Test
	public void testMainMethodPassesWithNumberNegativeNineHundredNinetyNineThousandNineHundredAndNinetyNine() {
		String expected = "negative nine hundred ninety nine thousand nine hundred ninety nine";
		String failMsg = "Unexpected response for -999999";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"-999999"}));
	}
	
	@Test
	public void testMainMethodPassesWith100() {
		String expected = "one hundred";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"100"}));
	}
	
	@Test
	public void testMainMethodPassesWithNegative100() {
		String expected = "negative one hundred";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"-100"}));
	}

	@Test
	public void testMainMethodPassesWith123() {
		String expected = "one hundred twenty three";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"123"}));
	}

	@Test
	public void testMainMethodPassesWith110() {
		String expected = "one hundred ten";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"110"}));
	}
	
	@Test
	public void testMainMethodPassesWith113() {
		String expected = "one hundred thirteen";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"113"}));
	}
	
	@Test
	public void testMainMethodPassesWithNegative123() {
		String expected = "negative one hundred twenty three";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"-123"}));
	}
	
	@Test
	public void testMainMethodPassesWith1001() {
		String expected = "one thousand one";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"1001"}));
	}
	
	@Test
	public void testMainMethodPassesWith1213() {
		String expected = "one thousand two hundred thirteen";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"1213"}));
	}
	
	@Test
	public void testMainMethodPassesWith12013() {
		String expected = "twelve thousand thirteen";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"12013"}));
	}
	
	@Test
	public void testMainMethodPassesWithNegative1000() {
		String expected = "negative one thousand";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"-1000"}));
	}
	
	@Test
	public void testMainMethodPassesWithCommas() {
		String expected = "one thousand";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"1,000"}));
	}	
	
	@Test
	public void testMainMethodPassesWithOneTwentyThreeThousand() {
		String expected = "one hundred twenty three thousand two hundred thirty three";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"123,233"}));
	}	
	
	@Test
	public void testMainMethodPassesWithOneTwentyThreeThousandFourFiftySix() {
		String expected = "one hundred twenty three thousand four hundred fifty six";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"123456"}));
	}
	
	@Test
	public void testMainMethodPassesWithZero() {
		String expected = "zero";
		String failMsg = "Did Not Return the Words Correctly";
		
		assertEquals(failMsg, expected, ConvertNumber.mainMethod(new String[]{"0"}));
	}
	
	
}

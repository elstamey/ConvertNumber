
public class ConvertNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println( mainMethod(args) );

	}
	
	public static String mainMethod(String[] args) {
		
		String outputString = "";
		
		if (args != null && args.length > 0) {
			
			Boolean isnegative = (args[0].startsWith("-"));
			
			args[0] = args[0].replace(",", "");
			
			char[] numbers;
			
			if (isnegative) {
				numbers = args[0].substring(1, args[0].length()).toCharArray();
			} else {
				numbers = args[0].toCharArray();
			}
			
			Integer numberValue;
			
			try {
				numberValue = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				numberValue = 1000000000;
			}
			
			
			if ((numberValue > -1000000) && (numberValue < 1000000)) {
				
				String hundredThousands = "";
				String tenThousands = "";
				String thousands = "";
				String hundreds = "";
				String tens = "";
				String ones = "";
				
				if (numbers.length > 0) {
					if (numbers.length == 1) {
						ones = "zero";
					} else {
						ones = numberNames( numbers[numbers.length - 1] );
					}
					
				}
				
				if (numbers.length > 1) {
					char temp = numbers[numbers.length - 2];
					if (temp == '1') {
						tens = numberTeensNames(temp, numbers[numbers.length - 1]);
						ones = "";
					} else if (temp != '0') {
						tens = numberTensNames(temp);
					}
					
				}
				
				if (numbers.length > 2) {
					char temp = numbers[numbers.length - 3];
					if (temp != '0') {
						hundreds = numberNames(temp) + "hundred ";
					}
				}
				
				if (numbers.length > 3) {
					char temp = numbers[numbers.length - 4];
					if (temp != '0') {
						thousands = numberNames(temp) + "thousand ";
					}
					
				}
				
				if (numbers.length > 4) {
					char temp = numbers[numbers.length - 5];
					if (temp == '1') {
						tenThousands = numberTeensNames(temp, numbers[numbers.length - 4]) + "thousand ";
						thousands = "";
					} else if (temp != '0') {
						tenThousands = numberTensNames(temp) + thousands;
						thousands = "";
					}
					
				}
				
				if (numbers.length > 5) {
					char temp = numbers[numbers.length - 6];
					if (temp != '0') {
						hundredThousands = numberNames(temp) + "hundred ";
					}
				}

				outputString = hundredThousands + tenThousands + thousands + hundreds 
						+ tens + ones;
				
				if (isnegative) {
					outputString = "negative " + outputString;
				}
			} else {
				outputString = "Please provide an integer between -999,999 and 999,999";
			}
			
			
		} else {
			outputString = "Please provide a number";
		}
		
		return outputString.trim();
	}
	
	private static String numberNames(char number) {
		String namedNumber = "";
		
		switch (number) {
			case '1': namedNumber = "one ";
			break;
			case '2': namedNumber = "two ";
			break;
			case '3': namedNumber = "three ";
			break;
			case '4': namedNumber = "four ";
			break;
			case '5': namedNumber = "five ";
			break;
			case '6': namedNumber = "six ";
			break;
			case '7': namedNumber = "seven ";
			break;
			case '8': namedNumber = "eight ";
			break;
			case '9': namedNumber = "nine ";
			break;
			
		}
		
		return namedNumber;
	}

	private static String numberTensNames(char number) {
		String namedNumber = "";
		
		switch (number) {
			case '2': namedNumber = "twenty ";
			break;
			case '3': namedNumber = "thirty ";
			break;
			case '4': namedNumber = "fourty ";
			break;
			case '5': namedNumber = "fifty ";
			break;
			case '6': namedNumber = "sixty ";
			break;
			case '7': namedNumber = "seventy ";
			break;
			case '8': namedNumber = "eighty ";
			break;
			case '9': namedNumber = "ninety ";
			break;
		
		}
		
		return namedNumber;
	}

	private static String numberTeensNames(char num1, char num2) {
		String namedNumber = "";
		
		String teenNum = String.valueOf(num1) + num2;
		
		switch (teenNum) {
			case "10": namedNumber = "ten ";
			break;
			case "11": namedNumber = "eleven ";
			break;
			case "12": namedNumber = "twelve ";
			break;
			case "13": namedNumber = "thirteen ";
			break;
			case "14": namedNumber = "fourteen ";
			break;
			case "15": namedNumber = "fifteen ";
			break;
			case "16": namedNumber = "sixteen ";
			break;
			case "17": namedNumber = "seventeen ";
			break;
			case "18": namedNumber = "eighteen ";
			break;
			case "19": namedNumber = "nineteen ";
			break;
		
		}
		
		return namedNumber;
	}

}

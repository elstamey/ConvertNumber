# ConvertNumber a java project

This was a coding exercise to take numeric inputs and return a string of the words used to describe those numbers.

Using the jar executable:

    > java -jar ConvertNumber.jar 100
    
You should receive a return value of:

    one hundred
    

**Language:** Java 

**Input:** Integer values from -999,999 to 999,999 

**Output:** String representation of the value 
